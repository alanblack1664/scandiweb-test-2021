<?php
namespace Forms;

/** 
* class FormFactory
* This class displays a form - and fields - for adding products.
*/

abstract class AbstractForm
{
   	public function displayFields() 
   	{
   			echo '<p id="sku">';
            echo '<label for="sku">SKU:</label>';
            echo '<input type="text" name="SKU" id="SKU" required oninvalid="this.setCustomValidity(\'SKU number is required\')" oninput="setCustomValidity(\'\')"';
            echo '</p>';
            echo '<p id="name">';
            echo '<label for="name">Name:</label>';
            echo '<input type="text" name="Name" id="Name" required oninvalid="this.setCustomValidity(\'Name is required\')" oninput="setCustomValidity(\'\')"';
            echo '</p>';
            echo '<p id="price">';
            echo '<label for="price">Price:</label>';
        	echo '<input type="text" name="Price" id="Price" required oninvalid="this.setCustomValidity(\'Price is required\')" oninput="setCustomValidity(\'\')"';
            echo '</p>';     

    $this->displaySpecificFields();
    }

    abstract public function displaySpecificFields();
}