<?php
namespace Forms;
use Forms\AbstractForm;

/** 
* class FormFactory
* This class produces forms to display the layout for the product add page.
*/

class FormFactory {

  public function getForm($form_type) {

    switch($form_type) {
      case 'Disc': 
      	return new SpecifiedForms\DiscForm;
      case 'Book': 
      	return new SpecifiedForms\BookForm;
      case 'Furniture': 
      	return new SpecifiedForms\FurnitureForm;
      default: 
      	throw new Exception('Invalid form was passed to FormFactory::getForm() !');
    }
  }



}
