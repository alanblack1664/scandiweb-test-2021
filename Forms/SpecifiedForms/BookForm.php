<?php
namespace Forms\SpecifiedForms;
use Forms\AbstractForm;

/** 
* class BookForm
* This class displays additional fields for the 'Book' product type
*/

class BookForm extends AbstractForm
{
	public function displaySpecificFields() 
	{
       echo '<p id="book">';
       echo 		'<label for="weight">Weight in grams:</label>';
       echo 		'<input type="number" name="Weight" id="Weight">';
       echo     	'<input type="hidden" name="ProductType" value="Book">';
       echo '</p>';
    }
}
