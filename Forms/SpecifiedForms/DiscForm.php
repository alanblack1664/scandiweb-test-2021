<?php
namespace Forms\SpecifiedForms;
use Forms\AbstractForm;

/** 
* class DiscForm
* This class displays additional fields for the 'Disc' product type
*/

class DiscForm extends AbstractForm
{
    public function displaySpecificFields() 
    {
       echo '<p id="DVD">';
       echo    		'<label for="size">Size in megabytes:</label>';
       echo     	'<input type="number" name="Size" id="Size">';
       echo     	'<input type="hidden" name="ProductType" value="Disc">';
       echo' </p>';
     }
}
