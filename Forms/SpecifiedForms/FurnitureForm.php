<?php
namespace Forms\SpecifiedForms;
use Forms\AbstractForm;

/** 
* class FurnitureForm
* This class displays additional fields for the 'Furniture' product type
*/

class FurnitureForm extends AbstractForm
{
	public function displaySpecificFields() {
        echo    '<p id="furniture">';
        echo    'Dimensions in centimeters:<br>';
        echo    '<br>';
        echo    '<label for="height">Height:</label>';
        echo    '<input type="number" name="Height" id="Height">';
        echo    '<label for="width">Width:</label>';
        echo    '<input type="number" name="Width" id="Width">';
        echo    '<label for="length">Length:</label>';
        echo    '<input type="number" name="Length" id="Length">';
        echo    '<input type="hidden" name="ProductType" value="Furniture">';
        echo    '</p>';
    }
}
