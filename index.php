<?php
include "autoloader.php";

use Logic\Action;
use Utils\ImageGenerator;
use Product\ProductDisplay;
$list = new Action;
$pic = new ImageGenerator;
$info = new ProductDisplay;
$row = $list->populateProductPage('POPULATE_PAGE');
?>
<h1 class="banner">Product listing</h1>

<?php require FILE_ROOT . '/View/Includes/head.php'; ?>
<?php require FILE_ROOT . '/View/Includes/navigation.php'; ?>

<body class="productlist" style="max-width:auto;max-height:auto">
<form method="post" action="Dispatch.php">
    <div class="grid-container">
        <div class="menu">Action:
            <input class="delete" type="submit" name="Delete" id="delete" value="Delete products">
        </div>
        <div class="productlist">
            <div class="d-flex flex-wrap" style="max-width:auto; max-height:auto;">
                <?php foreach($row as $item) : ?>
                    <div class="p-2">
                        <div class="card">
                        <p><input type="checkbox" class="checkbox" name='checkbox[]' value="<?php echo $item['SKU']; ?>"/><br></p>
                            <img class="card-img-top" src="<?php ImageGenerator::getRandomImage($item['SKU']); ?>">
                            <div class="card-body">
                                <h5 class="card-title"></h5>
                                <p class="card-text">
                                    <?php $info->displayProductValues($item);?>                                
                                </p>
                            </div>
                        </div>
                    </div>
                <?php endforeach?>
            </div>
        </div>
    </div>

</form>
</body>

</html>


