<?php

namespace Query;

/** 
*  class QueryFactory
*  A factory class that creates queries depending on the request for use with PDO.
*/

class QueryFactory {

    public function __construct() {}

    public function Selection() {
        return new SpecifiedQueries\SelectionQuery;
    }
    public function Insertion() {
        return new SpecifiedQueries\InsertionQuery;
    }
    public function Deletion() {
        return new SpecifiedQueries\DeletionQuery;
    }
    public function Check() {
        return new SpecifiedQueries\CheckQuery;
    }

}