<?php

/** 
*  class AbstractQuery
*  An abstract class used to generate a desired query for use with PDO.
*/

namespace Query;

abstract class AbstractQuery {

    public function __construct() {}

    abstract public function get($data=[]);

}