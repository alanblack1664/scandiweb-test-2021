<?php

namespace Query\SpecifiedQueries;
use Query\AbstractQuery;

/** 
*  class CheckQuery
*  A class that generates a generic SELECT query for use on the main page to display all products.
*/

class SelectionQuery extends AbstractQuery
{
    public function get($data=[])
    {
        return "SELECT * FROM products";
    }
}