<?php

namespace Query\SpecifiedQueries;
use Query\AbstractQuery;

/** 
*  class DeletionQuery
*  A class that generates a DELETE query for use on the main page for deleting checked products.
*/

class DeletionQuery extends AbstractQuery
{
    public function get($data=[])
    {
        return "DELETE FROM products WHERE SKU=\"". implode ('" OR SKU="', $data) . "\"";
    }   
}
