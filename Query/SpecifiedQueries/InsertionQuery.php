<?php

namespace Query\SpecifiedQueries;
use Query\AbstractQuery;

/** 
*  class CheckQuery
*  A class that generates an INSERT query for use in adding new products manually or via ProductNaker.
*/

class InsertionQuery extends AbstractQuery
{
    public function get($data=[])
    {
        $fields = array_keys($data);

        return 'INSERT INTO products ('.implode(', ', $fields).') VALUES
                (:'.implode(', :', $fields).');';
    }
}