<?php

namespace Query\SpecifiedQueries;
use Query\AbstractQuery;

/** 
*  class CheckQuery
*  A class that generates a SELECT query for use in checking for duplicate SKU values.
*/

class CheckQuery extends AbstractQuery
{
    public function get($data=[])
    {
        return 'SELECT COUNT(*) FROM products WHERE '.$data.' = :'.$data;

    }
}