<?php
include "autoloader.php";
use Utils\RandomProductGenerator\ProductMaker;

// A file that runs the ProductMaker so it can generate a random product.

$product = new ProductMaker;

$product->produce();

header('location:http://localhost/scandiweb/index.php');

