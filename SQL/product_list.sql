CREATE TABLE product_list(
	SKU varchar(40) NOT NULL, UNIQUE, PRIMARY KEY,
	Name varchar(40) NOT NULL,
	Price float(10,2) NOT NULL,

	Size int(18),

	Weight float(10,2),

	Height int(10),
	Width int(10),
	Length int(10),

	ProductType varchar(40) NOT NULL,

	PRIMARY KEY (SKU),
);
