<?php

namespace DB;

/** 
* class Database
* A wrapper class for PDO.
*/

use \PDO;

    define('DB_HOST', 'localhost');
    define('DB_USER', 'root'); 
    define('DB_PASS', ''); 
    define('DB_NAME', 'products');

    class Database {

        private $dbHost = DB_HOST;
        private $dbUser = DB_USER;
        private $dbPass = DB_PASS;
        private $dbName = DB_NAME;

        private $statement;
        private $dbHandler;
        private $error;

        public function __construct() {
            $conn = 'mysql:host=' . $this->dbHost . ';dbname=' . $this->dbName;
            $options = array(
                PDO::ATTR_PERSISTENT => true,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            );
            try {
                $this->dbHandler = new PDO($conn, $this->dbUser, $this->dbPass, $options);
            } catch (PDOException $e) {
                $this->error = $e->getMessage();
                echo $this->error;
            }
        }

        public function query($sql) {
            $this->statement = $this->dbHandler->prepare($sql);
        }

        public function bind($parameter, $value, $type = null) {
            switch (is_null($type)) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
            $this->statement->bindValue($parameter, $value, $type);
        }

        public function execute() {
            return $this->statement->execute();
        }
        public function countRows() {
            return $this->statement->rowCount();
        }
        public function fetchColumn() {
            return $this->statement->fetchColumn();
        }
        public function fetchSingleGeneric($fetch_type) {
            $this->execute();
            return $this->statement->fetch($fetch_type);
        }
        public function fetchSingleObject($object_type) {
            $this->execute();
            return $this->statement->fetchObject($object_type);
        }
        public function fetchAll($fetch_type) {
            $this->execute();
            return $this->statement->fetchAll($fetch_type);
        }
        public function fetchSingleOfType($fetch_type, $object_class = null) {
            $this->execute();
            return $this->statement->fetch($fetch_type, $object_class);
        }
        public function fetchAllOfType($fetch_type, $object_class = null) {
            $this->execute();
            return $this->statement->fetchAll($fetch_type, $object_class);
        }

}
