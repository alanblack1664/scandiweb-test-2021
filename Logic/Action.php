<?php
namespace Logic;

use Utils\DataProcessor;
use Logic\Operations;
use Query\QueryFactory;

/** 
* class Action
* This class prepares data for further dispatch; it does not interact with the database in any way.
*
*/


class Action {

    public $queryCaller;
    public $operationsCaller;

/*

*/
    public function __construct() {
        $this->queryCaller = new QueryFactory;
        $this->operationsCaller = new Operations;
    }

    /** 
    *  Takes a string request and creates the necessary Query object
    *  The valid requests are:
    *
    *   DELETE_PRODUCT,
    *   ADD_PRODUCT,
    *   SELECT_PRODUCT,
    *   POPULATE_PAGE,
    *   CHECK_IF_EXISTS.
    *
    *  @param string $requested_action
    *  @return object $request
    */

    public function processRequest($requested_action) {

        switch($requested_action) {
            case 'DELETE_PRODUCT':
                $request=$this->queryCaller->Deletion();
                return $request;
            case 'ADD_PRODUCT':
                $request=$this->queryCaller->Insertion();
                return $request;
            case 'SELECT_PRODUCT':
                $request=$this->queryCaller->Selection();
                return $request;
            case 'POPULATE_PAGE':
                $request=$this->queryCaller->Selection();
                return $request;
            case 'CHECK_IF_EXISTS':
                $request=$this->queryCaller->Check();
                return $request;
            default: 
                throw new Exception('An invalid action type has been passed to Action::processRequest() !');
            }
        }

    /** 
    *  Takes a Query object and an optional data array and generates a prepared query string.
    *
    *  @param object $query_object, nullable array $query_data_array
    *  @return string $request
    */


    public function generateQuery($query_object, $query_data_array=[]) {

        $prepared_query=$query_object->get($query_data_array);
        return $prepared_query;
    }

    /** 
    *  Takes a string value, processes request, generates a query, then calls an operation
    *  to check the database, and checks if the value exists in the database.
    *
    *  @param string $check_value
    *  @return bool $check
    */

    public function checkForDuplicates($check_value) {

        $check_request = self::processRequest('CHECK_IF_EXISTS');
        $check_query = self::generateQuery($check_request,'SKU');
        $check = $this->operationsCaller->checkIfSKUExists($check_value, $check_query);
        return $check;
    }


    /** 
    *  Takes an array and a requested action, procecesses the data, checks for duplicates
    *  in the array, then processes the requsted action, generates a query, then operates on the data.
    *   
    *
    *  @param array $raw_input_array, string $requested_action
    *  @return bool $action
    */

    public function doAction($raw_input_array, $requested_action) {

        $processed_input_data = DataProcessor::processArray($raw_input_array);

        $check = self::checkForDuplicates($processed_input_data['SKU']);
        if ($check == true) {
            throw new \Exception ('A product with SKU number '.$processed_input_data['SKU'].' already exists!');
        } else {

        $desired_query = self::processRequest($requested_action);
        $prepared_query = self::generateQuery($desired_query, $processed_input_data);
        $action = $this->operationsCaller->operate($processed_input_data, $prepared_query);

        return $action;
        }
    }

    /** 
    *  Takes a string value, processes request, generates a query, then calls an operation
    *  to check the database, and fetches a nested associative array of the product table.
    *
    *  @param string $requested_action
    *  @return array $list
    */

    public function populateProductPage($requested_action) {
        
        $desired_query = self::processRequest($requested_action);
        $prepared_query = self::generateQuery($desired_query);
        $list = $this->operationsCaller->populateProductPage($prepared_query);

        return $list;
    }

}