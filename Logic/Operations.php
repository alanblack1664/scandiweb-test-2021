<?php
namespace Logic;

use DB\Database;
use \PDO;

class Operations {
    private $db;
    public function __construct() {
        $this->db = new Database;
    }

    /** 
    *  Takes an SKU value and a query, binds and executes, then fetches columns.
    *  Returns true if exists, false if otherwise.
    *
    *  @param string $SKU, string $query
    *  @return bool
    */

    public function checkIfSKUExists($SKU,$query) {

        $this->db->query($query);

        $this->db->bind(':SKU', $SKU);
        $this->db->execute();

        if($this->db->fetchColumn() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /** 
    *  Takes a data array and a prepared query, binds each parameter, then executes.
    *  Returns true if successful, false if otherwise.
    *
    *  @param array $data, string $query
    *  @return bool
    */



    public function operate($data, $query) {
        $this->db->query($query);

        foreach ($data as $key => $value) {
            $this->db->bind(':'.$key, $data[$key]);
        }

        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /** 
    *  Takes a prepared query and fetches an associative array.
    *
    *  @param string $query
    *  @return array $action
    */

    public function populateProductPage($query) {
        $this->db->query($query);

        $page = $this->db->fetchAll(PDO::FETCH_ASSOC); 
        return $page;
    }




}