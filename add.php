<?php

include "autoloader.php";
use Forms\FormFactory;
$factory = new FormFactory;
?>

<h1 class="banner">Add products</h1>
<?php require FILE_ROOT . '/View/Includes/head.php'; ?>
<?php require FILE_ROOT . '/View/Includes/navigation.php'; ?>

<body class="productadd" style="max-width:auto;max-height:auto">

<div class="product-add-container">
    <div class="dropdown">
        <button class="dropbutton">Select product type:</button>
        <div class="dropdown-content">
           <a href="productadd.php?type=Book">Books</a>
           <a href="productadd.php?type=Disc">Discs</a>
           <a href="productadd.php?type=Furniture">Furniture</a>
        </div>
    </div>
<div class="add-product-menu">
    <form action="Dispatch.php" method="post">
        <?php 
        $form = $factory->getForm($_GET['type'] ?? 'Book');
        $form->displayFields();
        ?>
        <input type="submit" value="Add product">
    </form>

</div>


</body>
</html>