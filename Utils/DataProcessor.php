<?php
namespace Utils;

/* 
*  class DataProcessor
*  
*  Used to parse an unsorted array.
*  As this loop was repeated in a few other places, may make sense to have it be a generic method.
*/

class DataProcessor {

	public function __construct() {}

	public static function processArray($input_array) {

		$output = [];

    	foreach ($input_array as $key => $value) {
    	        $output[$key] = $value;
    	}

    	return $output;
	}
}