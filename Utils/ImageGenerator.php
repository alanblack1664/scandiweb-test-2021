<?php
namespace Utils;

/* 
*  class ImageGenerator
*  Used to generate placeholder images from picsum.
*  Currently uses the SKU to generate a placeholder for the sake of identifying what's what.
*/

class ImageGenerator {

	public function __construct() {}

	public function getRandomImage($ID) {

    echo "https://picsum.photos/id/",$ID,"/175/200/";

	}

}
