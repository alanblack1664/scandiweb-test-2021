<?php
namespace Utils\RandomProductGenerator\Dictionaries;

/** 
*  class AbstractDictionary
*  An abstract class for making dictionaries for each product type.
*/

abstract class AbstractDictionary {

    public function __construct() {}

    abstract public function getList();

    abstract public function getRandomNameFromList();

}