<?php

namespace Utils\RandomProductGenerator\Dictionaries;

/** 
*  class DictionaryFactory
*  A factory class that produces dictionaries for each product type.
*/

class DictionaryFactory {

    public function __construct() {}

    public function BookDictionary() {
        return new SpecifiedDictionaries\BookDictionary;
    }
    public function DiscDictionary() {
        return new SpecifiedDictionaries\DiscDictionary;
    }
    public function FurnitureDictionary() {
        return new SpecifiedDictionaries\FurnitureDictionary;
    }

}