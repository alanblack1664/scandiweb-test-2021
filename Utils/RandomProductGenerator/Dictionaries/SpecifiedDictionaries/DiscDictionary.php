<?php
namespace Utils\RandomProductGenerator\Dictionaries\SpecifiedDictionaries;
use Utils\RandomProductGenerator\Dictionaries\AbstractDictionary;

/** 
*  class DiscDictionary
*  A dictionary for the 'Disc' product type.
*/

class DiscDictionary extends AbstractDictionary {

	public $discNames = [
		"The Witcher 3 - Wild Hunt",
		"Team Fortress 2",
		"Die Hard",
		"Death Stranding",
		"Cyberpunk 2077",
		"Mount and Blade: Warband",
		"Company of Heroes",
		"Terminator",
		"Lord of the Rings",
		"Home Alone",
		"Naruto",
		"Payday 2",
		"Heroes and Generals",
		"Raid: Shadow Legends",
		"Hearts of Iron 4",
		"Europa Universalis 4",
		"Minecraft",
		"Grand Theft Auto 5",
		"Tropico 5",
		"Risen 3: Titan Lords",
		];
		
	/** 
	*  Returns the list of valid names.
	*  @return array
	*/

	public function getList() {
		return $this->discNames;
	}

	/** 
	*  Parses the list of names and returns a random name.
	*  @return string
	*/

	public function getRandomNameFromList() {

		$names = $this->discNames;

		// We use sizeof() - 1 to prevent accessing an invalid offset.
		// This way the random number generator will be able to scale with the array.
		// A hack, but for its intended application it is not critical.

		$keycount = sizeof($names) - 1;
		$randkeys = rand(0,$keycount);

		return $names[$randkeys];
	}
	
}