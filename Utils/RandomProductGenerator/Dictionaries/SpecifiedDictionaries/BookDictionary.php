<?php
namespace Utils\RandomProductGenerator\Dictionaries\SpecifiedDictionaries;
use Utils\RandomProductGenerator\Dictionaries\AbstractDictionary;

/** 
*  class BookDictionary
*  A dictionary for the 'Book' product type.
*/

class BookDictionary extends AbstractDictionary {

	public $bookNames = [
		"In Search Of Lost Time",
		"Ulysses",
		"Don Quixote",
		"100 Years of Solitude",
		"The Great Gatsby",
		"Moby Dick",
		"War and Peace",
		"Hamlet",
		"The Odyssey",
		"Madame Bovary",
		"The Divine Comedy",
		"The Brothers Karamazov",
		"Crime and Punishment",
		"The Catcher in the Rye",
		"Pride and Prejudice",
		"The Adventures of Huckleberry Finn",
		"Anna Karenina"
		];
		
	/** 
	*  Returns the list of valid names.
	*  @return array
	*/

	public function getList() {
		return $this->bookNames;
	}

	/** 
	*  Parses the list of names and returns a random name.
	*  @return string
	*/

	public function getRandomNameFromList() {

		$names = $this->bookNames;
		// We use sizeof() - 1 to prevent accessing an invalid offset.
		// This way the random number generator will be able to scale with the array.
		// A hack, but for its intended application it is not critical.
		$keycount = sizeof($names) - 1;
		$randkeys = rand(0,$keycount);

		return $names[$randkeys];
	}
	
}