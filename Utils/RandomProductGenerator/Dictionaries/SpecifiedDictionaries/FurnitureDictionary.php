<?php
namespace Utils\RandomProductGenerator\Dictionaries\SpecifiedDictionaries;
use Utils\RandomProductGenerator\Dictionaries\AbstractDictionary;

/** 
*  class FurnitureDictionary
*  A dictionary for the 'Furniture' product type.
*/

class FurnitureDictionary extends AbstractDictionary {

	public $furnitureNames = [
		"Table",
		"Chair",
		"Shelving",
		"Closet",
		"Bed",
		"Couch",
		"Ottoman",
		"Stool",
		"Bench",
		"Hammock",
		"Pool Table",
		"Counter-top",
		"Desk",
		"Workbench",
		"Bookcase",
		"Chest",
		"Drawer",
		"Wardrobe"
		];

	/** 
	*  Returns the list of valid names.
	*  @return array
	*/

	public function getList() {
		return $this->furnitureNames;
	}

	/** 
	*  Parses the list of names and returns a random name.
	*  @return string
	*/

	public function getRandomNameFromList() {

		$names = $this->furnitureNames;

		// We use sizeof() - 1 to prevent accessing an invalid offset.
		// This way the random number generator will be able to scale with the array.
		// A hack, but for its intended application it is not critical.

		$keycount = sizeof($names) - 1;
		$randkeys = rand(0,$keycount);
		
		return $names[$randkeys];
	}
	
}