<?php
namespace Utils\RandomProductGenerator;
use Logic\Action;
use Utils\RandomProductGenerator\Dictionaries\DictionaryFactory;

/* 
*  class ProductMaker
*
*  Used to generate a completely random product for debugging purposes.
*  Not 100% necessary to include, but it helps to test and is much faster than 
*  smashing one's face on the keyboard to commit random products directly.
*/

class ProductMaker {

    public $library;
    public $dictionary;

    public $productTypes = [
        "Book", 
        "Disc", 
        "Furniture"
    ];

    public $baseParams = [
        "SKU" => '',
        "Name" => '',
        "Price" => ''
    ];
    public $bookParams = [
        "Weight" => '', 
        "ProductType" => "Book"
    ];
    public $discParams = [
        "Size" => '', 
        "ProductType" => "Disc"
    ];

    public $furnitureParams = [
        "Height" => '',
        "Width" => '', 
        "Length" => '',
        "ProductType" => "Furniture"
    ];



    public function __construct() {
        // This is used once to later instantiate a dictionary for each product type.
        // A dictionary plays host to a list of valid names for each product type.
        $this->library = new DictionaryFactory;
    }

    /** 
    *  Accesses the productTypes property to get a list of valid product types.
    *  Returns a string - a member of an array.
    *
    *  @return string
    */


    public function getRandomProductType() {
        $names = $this->productTypes;
        $keycount = sizeof($names) - 1;
        $randkeys = rand(0,$keycount);
        return $names[$randkeys];
    } 

    /** 
    *  Calls the above method to fetch a random product type.
    *  Once a type is given, it creates a new empty array of base parameters and 
    *  specified parameters, then it generates an appropriate dictionary.
    *
    *  @return array
    */

    public function setupRandomProductTemplate() {
        $type = self::getRandomProductType();
        switch($type) {
            case "Book":
            $template = array_merge($this->baseParams, $this->bookParams);
            $this->dictionary = $this->library->BookDictionary();
            return $template;

            case "Disc":
            $template = array_merge($this->baseParams, $this->discParams);
            $this->dictionary = $this->library->DiscDictionary();
            return $template;

            case "Furniture":
            $template = array_merge($this->baseParams, $this->furnitureParams);
            $this->dictionary = $this->library->FurnitureDictionary();
            return $template;   

            default:
                throw new Exception ('An invalid product type has been generated by ProductMaker::getRandomProductType() !');        
        }
    }

    /** 
    *  Populates an array produced by setupRandomProductTemplate() with random values.
    *
    *  @return array
    */

    public function populateRandomProductTemplate(array $prepared_array) {

        $output = [];
        foreach ($prepared_array as $key => $value) {
            // ProductType cannot be overwritten, so the loop must skip it.
            if ($key == 'ProductType') continue;
                $output[$key] = rand(1,450);
        }

        // Once the loop has populated the array - get a name from the appropriate dictionary.
        // While this could be done in the beginning, and an exception could be added alongside 
        // ProductType so it would not override the namr, for this specific circumstance it's fine.

        $output['Name'] = $this->dictionary->getRandomNameFromList();
        return $output;
    }

    /** 
    *  Generates an array of data which corresponds to a random product with randomized values.
    *
    *  @return array
    */

    public function generateProduct() {
        $template = self::setupRandomProductTemplate();
        $product = self::populateRandomProductTemplate($template);
        return $product;
    }

    /** 
    *  Generates an array of random data for a random product, then calls an Action
    *  to include it in the database.
    */

    public function produce() {
        $product = self::generateProduct();
        $act = new Action;
        $act->doAction($product, 'ADD_PRODUCT');
    }


}