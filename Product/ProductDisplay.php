<?php

namespace Product;

/*

This class is solely meant to process the SQL-fetched array into presentable data.


*/

class ProductDisplay {

    public function __construct() {}

    /** 
    *  Returns a string based on the 'ProductType' key of the product table.
    *
    *  @param string $property
    *  @return string 
    */

    public function generatePropertyTag(string $property) {
        // If the input is any of the following, it will return the correct tag.
        switch($property) {
                case 'Price': 
                    return 'Euros';
                case 'Weight': 
                    return 'g';
                case 'Size': 
                    return 'MB';
                case 'Width':
                case 'Height':
                case 'Length': 
                    return "cm";
        // Otherwise it simply breaks.
                default: 
                    break;
        }
    }

    /** 
    *  Sorts the SQL row array, excludes ProductType and SKU values, 
    *  trims null values, then returns a clean array
    *
    *  @param array $input
    *  @return array $output 
    */

    public function filterProductArray(array $input) {

        $output = [];
        foreach ($input as $key => $value) {
            // If the following keys are reached - do not include. We don't want to display them.
            if ($key == 'ProductType' || $key == 'SKU') continue;
            // Instead of having to handle each product type separately, it simply filters out the null values.
            if (!is_null($value)) {
                $output[$key] = $value;
            }
        }
        return $output;
    }

    /** 
    *  Takes a raw SQL array, processes it via the above method, then iterates on 
    *  each member to generate a string for each key and value.
    *
    *  @param array $input
    */

    public function displayProductValues(array $product_array) {
        $input = self::filterProductArray($product_array);
        foreach ($input as $key => $value) {
            echo $key.' = '.$value.' '.self::generatePropertyTag($key).'<br>';
            }
        }

}



